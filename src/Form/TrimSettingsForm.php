<?php

namespace Drupal\trim\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * A Class for TrimSettingsForm.
 */
class TrimSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs an SubscriptionAPICredentialForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'trim.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trim_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('trim.settings');
    $form['content_type'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Exclude content types from Trim.'),
      '#options' => $this->getAllContentTypeInfo(),
      '#default_value' => !empty($config->get('content_type')) ? $config->get('content_type') : [],
      '#description' => $this->t("Selected content types will be excluded from processing of the trim module."),
    ];

    $form['vocabularies'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Exclude Vocabularies from Trim.'),
      '#options' => $this->getAllVocabularyInfo(),
      '#default_value' => !empty($config->get('vocabularies')) ? $config->get('vocabularies') : [],
      '#description' => $this->t("Selected Vocabularies will be excluded from processing of the trim module."),
    ];

    $form['users'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Exclude Users from Trim.'),
      '#options' => [
        'user' => $this->t('User'),
      ],
      '#default_value' => !empty($config->get('users')) ? $config->get('users') : [],
      '#description' => $this->t("All users will be excluded from processing of the trim module."),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Get content type info.
   */
  public function getAllContentTypeInfo() {
    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($content_types as $content_type) {
      $types[$content_type->id()] = $content_type->label();
    }
    return $types;
  }

  /**
   * Get vocabularies info.
   */
  public function getAllVocabularyInfo() {
    $vocabularies = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();
    foreach ($vocabularies as $vocabularie) {
      $types[$vocabularie->id()] = $vocabularie->label();
    }
    return $types;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('trim.settings');
    $config->set('content_type', $form_state->getValue('content_type'));
    $config->set('vocabularies', $form_state->getValue('vocabularies'));
    $config->set('users', $form_state->getValue('users'));
    $config->save();
  }

}
